## Clem
An emacs-like editor that integrates clang-based static code analysis.

## Features
Clem provides a clear interface for terminal operations and buffer operations,
and makes it very easy to add your own plugins.