//
// Created by tzhou on 1/2/18.
//

#include <iostream>
#include "Buffer.hpp"
#include "Editor.hpp"

typedef std::string string;

BufLine::BufLine() {
  _data = string(Editor::_E->getMaxLineLength(), '\0');
}

BufLine* Buffer::getLine(size_t i) {
  return _lines.at(i);
}

void Buffer::insertLine(std::string line) {
  auto bl = new BufLine();
  bl->str().insert(0, line);
  _lines.push_back(bl);
}