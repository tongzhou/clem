//
// Created by tzhou on 1/1/18.
//

#include "Editor.hpp"
#include "ANSITerm.hpp"
#include "Errors.hpp"
#include "Window.hpp"
#include "Keys.hpp"
#include "Buffer.hpp"

Editor* Editor::_E = nullptr;
ANSITerm* Editor::_T = nullptr;
FILE* Editor::_Log = nullptr;

void Editor::init() {
  Errors::init();
  _E = new Editor();
  _T = new ANSITerm();
  _Log = fopen("clem.log", "w");
  _E->initialze();
}

void Editor::destroy() {
  _E->finalize();
  delete Editor::_E;
  delete _T;
  fclose(_Log);
}

Editor::Editor() {
  _cx = 0;
  _cy = 0;
}

Editor::~Editor() {

}

void Editor::initialze() {
  _T->enableRawMode();
  _T->clearScreen();

  auto b = new Buffer();
  _buffers.push_back(b);

  auto w = new Window();
  _windows.push_back(w);
  w->setBuffer(b);
  w->setSize(0, _T->getWidth(), 0, _T->getHeight());
  w->drawTildes();
  _T->moveCursorTo(0, 0);
}

void Editor::finalize() {
  _T->disableRawMode();
  for (auto w: _windows) {
    delete w;
  }
}

Window* Editor::getActiveWindow() {
  return _windows[0];
}

Buffer* Editor::getActiveBuffer() {
  return _buffers[0];
}

void Editor::loop() {
  auto B = getActiveBuffer();
  auto W = getActiveWindow();
  B->insertLine("hello");
  while (true) {
    W->refresh();
    processKeypress();
  }
}

#define CTRL_KEY(k) ((k) & 0x1f)
void Editor::processKeypress() {
  auto W = getActiveWindow();
  int k = Keys::readKey();
  switch (k) {
  case CTRL_KEY('q'):
    write(STDOUT_FILENO, "\x1b[2J", 4);
    write(STDOUT_FILENO, "\x1b[H", 3);
    exit(0);
    break;
  case Keys::ARROW_UP:
  case Keys::ARROW_DOWN:
  case Keys::ARROW_LEFT:
  case Keys::ARROW_RIGHT:
    moveCursor(k);
    break;
  }
}


void Editor::moveCursor(int key) {
  auto W = getActiveWindow();
  switch (key) {
  case Keys::ARROW_LEFT:
    if (_cx > W->_min_x)
      _cx--;
    break;
  case Keys::ARROW_RIGHT:
    if (_cx < W->getMaxX())
      _cx++;
    break;
  case Keys::ARROW_UP:
    if (_cy > W->_min_y)
      _cy--;
    break;
  case Keys::ARROW_DOWN:
    if (_cy < W->getMaxY())
      _cy++;
    break;
  }
}

void Editor::renderCursor() {
  Editor::_T->moveCursorTo(_cx, _cy);
}

void Editor::moveCursorLeft() {
  _cx--;
  _T->moveCursorTo(_cx, _cy);
}

void Editor::moveCursorRight() {
  _cx++;
  _T->moveCursorTo(_cx, _cy);
}