//===----------------------------------------------------------------------===//
///
/// \file
/// ANSITerm.cpp
///
//===----------------------------------------------------------------------===//

#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <iostream>
#include <cassert>
#include <Errors.hpp>
#include <sys/ioctl.h>

#include "ANSITerm.hpp"

typedef std::string string;

ANSITerm::ANSITerm() {
  _log_read_byte = true;

  if (_log_read_byte) {
    _byte_log = fopen("bytes.log", "w");
  }
  getWindowSize(&_nrow, &_ncol);
}

ANSITerm::~ANSITerm() {
  if (_log_read_byte) {
    fclose(_byte_log);
  }
  //printf("to exit\n");
}

void ANSITerm::enableRawMode() {
  int ret = tcgetattr(STDIN_FILENO, &_orig_termios);
  guarantee(ret != -1, "tcgetattr failed");

  termios raw = _orig_termios;
  // Disabled bits
  // ECHO:
  //   Print on the screen what you have entered
  // ICANON:
  //   Canonical mode, input is only read after hitting ENTER
  //   so that user can do backspace etc.
  // ISIG:
  //   Handle Ctrl-C, Ctrl-Z as signal. On MacOS also Ctrl-Y.
  // IXON:
  //   By default, Ctrl-S and Ctrl-Q
  //   are used for software flow control. Ctrl-S stops data from
  //   being transmitted to the terminal until you press Ctrl-Q.
  //   This originates in the days when you might want to pause
  //   the transmission of data to let a device like a printer
  //   catch up.
  // IEXTEN:
  //   On some systems, when you type Ctrl-V, the terminal waits
  //   for you to type another character and then sends that
  //   character literally.
  // ICRNL:
  //   Ctrl-M and Enter will produce 10 (new line), instead of 13.
  // OPOST:
  //   Translate "\n" to a "\r\n" (move the cursor to the beginning
  //   of the line, then move down a line)

  raw.c_lflag &= ~(ECHO | ICANON | ISIG | IXON | IEXTEN | ICRNL);
  raw.c_oflag &= ~(OPOST);
  // Miscellaneous flags that don’t really apply to modern terminal
  // emulators. Turn them off for the raw mode tradition.
  raw.c_iflag &= ~(BRKINT | INPCK | ISTRIP);
  raw.c_cflag |= (CS8);
  // The min number of bytes of input needed before read() can return
  raw.c_cc[VMIN] = 0;
  // The max amount of time to wait before read() returns.
  // It is in tenths of a second. In such case, read() returns 0.
  raw.c_cc[VTIME] = 1;

  ret = tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
  guarantee(ret != -1, "tcsetattr failed");
}

void ANSITerm::disableRawMode() {
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &_orig_termios);
}

ssize_t ANSITerm::readByte(char* cp, FILE * file) {
  ssize_t nbytes = read(STDIN_FILENO, cp, 1);  /* return 0 when timeout */
  if (_log_read_byte) {
    if (nbytes == 1) {
      if (iscntrl(*cp)) {
        fprintf(_byte_log, "%d\n", *cp);
      } else {
        fprintf(_byte_log, "%d ('%c')\n", *cp, *cp);
      }
      fflush(_byte_log);
    }
  }

  return nbytes;
}

void ANSITerm::getWindowSize(int *nrow, int *ncol) {
  winsize ws;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
    guarantee(0, "The ioctl way of getting window size may not be supported");
  }
  else {
    *ncol = ws.ws_col;
    *nrow = ws.ws_row;
  }
}

void ANSITerm::clearScreen() {
  write(STDIN_FILENO, "\x1b[2J", 4);
  moveCursorTo(0, 0);
}

void ANSITerm::moveCursorTo(int x, int y) {
  char cmd[64] = {0};
  sprintf(cmd, "\x1b[%d;%dH", y+1, x+1);
  write(STDIN_FILENO, cmd, strlen(cmd));
}

