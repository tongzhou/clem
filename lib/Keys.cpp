//
// Created by tzhou on 1/1/18.
//

#include <unistd.h>
#include "Keys.hpp"
#include "ANSITerm.hpp"
#include "Editor.hpp"
#include "Errors.hpp"

// int Keys::read(int fd) {
//   auto T = Editor::_T;
//   int nread;
//   char c;
//   while ((nread = T->readByte(&c)) != 1) {
//     if (nread == -1 && errno != EAGAIN) {
//       guarantee(0, "");
//     }
//   }
//   if (c == '\x1b') {
//     char seq[3];
//     if (read(STDIN_FILENO, &seq[0], 1) != 1) return '\x1b';
//     if (read(STDIN_FILENO, &seq[1], 1) != 1) return '\x1b';
//     if (seq[0] == '[') {
//       switch (seq[1]) {
//       case 'A': return ARROW_UP;
//       case 'B': return ARROW_DOWN;
//       case 'C': return ARROW_RIGHT;
//       case 'D': return ARROW_LEFT;
//       }
//     }
//     return '\x1b';
//   } else {
//     return c;
//   }
// }

int Keys::readKey(int fd) {
  auto T = Editor::_T;
  int nread;
  char c, seq[3];
  while ((nread = T->readByte(&c)) == 0);
  if (nread == -1) exit(1);

  while(1) {
    switch(c) {
    case ESC:    /* escape sequence */
      /* If this is just an ESC, we'll timeout here. */
      if (T->readByte(seq) == 0) return ESC;
      if (T->readByte(seq+1) == 0) return ESC;

      /* ESC [ sequences. */
      if (seq[0] == '[') {
        if (seq[1] >= '0' && seq[1] <= '9') {
          /* Extended escape, read additional byte. */
          if (T->readByte(seq+2) == 0) return ESC;
          if (seq[2] == '~') {
            switch(seq[1]) {
            case '1': return HOME_KEY;  // Esc[1~
            case '3': return DEL_KEY;
            case '4': return END_KEY;
            case '5': return PAGE_UP;
            case '6': return PAGE_DOWN;
            case '7': return HOME_KEY;
            case '8': return END_KEY;
            }
          }
        } else {
          switch(seq[1]) {
          case 'A': return ARROW_UP;  // Esc[A
          case 'B': return ARROW_DOWN;
          case 'C': return ARROW_RIGHT;
          case 'D': return ARROW_LEFT;
          case 'H': return HOME_KEY;
          case 'F': return END_KEY;
          }
        }
      }

      /* ESC O sequences. */
      else if (seq[0] == 'O') {
        switch(seq[1]) {
        case 'H': return HOME_KEY;
        case 'F': return END_KEY;
        }
      }
      break;
    default:
      return c;
    }
  }
}
