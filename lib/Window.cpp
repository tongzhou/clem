//
// Created by tzhou on 1/1/18.
//

#include <cassert>
#include <Errors.hpp>
#include <Buffer.hpp>
#include <iostream>
#include <cstring>
#include "Window.hpp"
#include "Editor.hpp"
#include "ANSITerm.hpp"

Window::Window() {
  setSize(0, 0, 0, 0);
  _buffer = NULL;
}

Window::~Window() {

}

void Window::drawTildes() {
  for (int r = _min_y; r < _height; r++) {
    if (r < _height - 1) {
      printf("~\r\n");
    }
    else {
      printf("~\r");
    }
  }
  fflush(stdout);
}

void Window::setSize(int x, int width, int y, int height) {
  _min_x = x;
  _min_y = y;
  _width = width;
  _height = height;
}

void Window::refresh() {
  guarantee(_buffer, "");
  auto B = _buffer;
  Editor::_E->saveCursor();
  resetCursor();

  // A buffer measures in line and a window measures in row
  // One buffer line could be rendered as multiple rows
  size_t nrow = B->getLineNum();
  for (int r = _min_y; r < _height; r++) {
    if (r < nrow) {  /* text part */
      BufLine* line = B->getLine(r);
      write(STDOUT_FILENO, line->c_str(), strlen(line->c_str()));

      //fprintf(Editor::_Log, "line len: %d\n", strlen(line->c_str()));
      //printf("line: %s", line->c_str());
    }
//    else {  /* empty part */
//      if (r < _height - 1) {
//        printf("~\r\n");
//      }
//      else {
//        printf("~\r");
//      }
//    }
  }
  Editor::_E->restoreCursor();
  Editor::_E->renderCursor();
  fflush(stdout);
}

void Window::resetCursor() {
  Editor::_T->moveCursorTo(_min_x, _min_y);
}