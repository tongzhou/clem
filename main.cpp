#include <iostream>
#include <Editor.hpp>

int main() {
  Editor::init();

  auto E = Editor::_E;
  E->loop();

  Editor::destroy();
//  auto term = new ANSITerm();
//  term->enableRawMode();
//  term->clearScreen();
//
//  while (true) {
//    char c;
//    ssize_t nbytes = term->readByte(&c);
//    if (c == 'q') {
//
//      break;
//    }
//  }
//
//  term->disableRawMode();
//  delete term;
  return 0;
}
