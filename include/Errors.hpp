//
// Created by tzhou on 12/31/17.
//

#ifndef KILO_ERRORS_HPP
#define KILO_ERRORS_HPP

#ifdef __linux__
#include <execinfo.h>
#endif
#include <signal.h>
#include <stdexcept>


class Errors {
public:
  static FILE* log;

  static void init();
  static void install_sig_handlers();
  static void uninstall_sig_handlers();
  static void semantic_error_handler();
  static void sigsegv_handler(int sig, siginfo_t *siginfo, void *context);
  //static void sigsegv_handler(int sig);
  static void print_backtrace_symbols();
  static void die();
};

#define guarantee(condition, args...) \
    do { \
        if (! (condition)) { \
            fflush(stdout); \
            fprintf(stderr, "Assertion `" #condition "` failed in %s:%d: ", __FILE__, __LINE__); \
            fprintf(stderr, ##args); \
            fprintf(stderr, "\n"); \
            Errors::semantic_error_handler(); \
        } \
    } while (false)



#endif //KILO_ERRORS_HPP
