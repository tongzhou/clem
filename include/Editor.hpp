//
// Created by tzhou on 1/1/18.
//

#ifndef CLEM_EDITOR_HPP
#define CLEM_EDITOR_HPP

#include <vector>
#include <cstdio>

class ANSITerm;
class Window;
class Buffer;

class Editor {
  std::vector<Window*> _windows;
  std::vector<Buffer*> _buffers;
  int _cx;
  int _cy;
  int _cx_backup;
  int _cy_backup;
public:

public:
  Editor();
  ~Editor();
  void initialze();
  void finalize();
  Window* getActiveWindow();
  Buffer* getActiveBuffer();
  void loop();
  void moveCursor(int key);
  void processKeypress();
  int getMaxLineLength()            { return 84; }
  void saveCursor()                 { _cx_backup = _cx; _cy_backup = _cy; }
  void restoreCursor()              {
    _cx = _cx_backup; _cy = _cy_backup;
  }
  void renderCursor();
  void moveCursorLeft();
  void moveCursorRight();
public:
  static void init();
  static void destroy();
  static Editor* _E;
  static ANSITerm* _T;
  static FILE* _Log;
};


#endif //CLEM_EDITOR_HPP
