//
// Created by tzhou on 1/1/18.
//

#ifndef CLEM_TERMINAL_HPP
#define CLEM_TERMINAL_HPP

class TerminalBase {
public:
  int _nrow;
  int _ncol;
public:
  virtual void enableRawMode()=0;
  virtual void disableRawMode()=0;
  int getMaxRowNum()                     { return _nrow; }
  int getMaxColNum()                     { return _ncol; }
};

#endif //CLEM_TERMINAL_HPP
