//
// Created by tzhou on 1/1/18.
//

#ifndef CLEM_WINDOW_HPP
#define CLEM_WINDOW_HPP

class Buffer;

class Window {
public:
  int _min_x;
  int _min_y;
  int _width;
  int _height;
  Buffer* _buffer;

  Window();
  ~Window();

  void drawTildes();
  void setSize(int x, int width, int y, int height);
  int getMaxX()               { return _min_x + _width - 1; }
  int getMaxY()               { return _min_y + _height - 1; }
  void setBuffer(Buffer* b)   { _buffer = b; }
  void refresh();
  void resetCursor();
};

#endif //CLEM_WINDOW_HPP
