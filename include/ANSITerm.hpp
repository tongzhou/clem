//
// Created by tzhou on 1/1/18.
//

#ifndef CLEM_ANSITERM_HPP
#define CLEM_ANSITERM_HPP

#include <fstream>
#include <termios.h>
#include <unistd.h>
#include "Terminal.hpp"

class ANSITerm: public TerminalBase {
  termios _orig_termios;

public:
  FILE* _byte_log;
  bool _log_read_byte;
public:
  ANSITerm();
  ~ANSITerm();
  void enableRawMode();
  void disableRawMode();
  ssize_t readByte(char* c, FILE* file=STDIN_FILENO);
  void clearScreen();
  void moveCursorTo(int row, int col);
  int getHeight()            { return _nrow; }
  int getWidth()             { return _ncol; }
  static void getWindowSize(int* nrow, int* ncol);
};

#endif //CLEM_ANSITERM_HPP
