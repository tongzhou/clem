//
// Created by tzhou on 1/2/18.
//

#ifndef CLEM_BUFFER_HPP
#define CLEM_BUFFER_HPP

#include <cstddef>
#include <string>
#include <vector>

class BufLine {
public:
  size_t _end;
  std::string _data;
public:
  BufLine();
  std::string& str()                     { return _data; }
  const char* c_str()                    { return _data.c_str(); }
};

class Buffer {
public:
  size_t _linum;
  std::vector<BufLine*> _lines;
public:
  size_t getLineNum()                    { return _lines.size(); }
  BufLine* getLine(size_t i);
  void insertLine(std::string line);
};

#endif //CLEM_BUFFER_HPP
